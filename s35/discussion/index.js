const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// change the admin password:
/*
	MongoDB Atlas > Database Access (Left navigation pane) > look for the admin user > change password
*/

// mongoose.connect - allows our application to be connected to MongoDB
// userNewUrlParser : true - allows us to avoid any current and future errors while connecting to MongoDB
// useUnifiedTopology : true - it allows us to connect to MongoDB even if the required IP address is updated
mongoose.connect("mongodb+srv://admin:admin123@zuitt.zwh8fik.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// connection to the database
// set notifications if the connection is a success or failure
let db = mongoose.connection;
// if a connection error occured, output in the console
// "console.error.bind(console)" allows us to print errors in  the browser console as well as in the terminal
// "connection error" is the message that will display if this happens
db.on("error", console.error.bind(console, "connection error"))
// if the connection is successful, output in the console: "We are connected to the database"
db.once("open", () => console.log("We are connected to the database"));


// [ SECTION ] Mongoose Schema 
// Schemas determine the structure of documents to be written in the database; they act as blueprints of our data
/*
	Syntax:
		Use the Schema() constructor of the mongoose module to create a new Schema object
*/
const taskSchema = new mongoose.Schema({
	// define the fields with corresponding data type
	// ex. it needs a task "name" and task "status"
	// the "name" field requires a String data type for its value
	name : String,
	// "status" requires a String data type, but since we have (default : "pending"), users can leave this blank with a default value of "pending"
	status : {
		type : String,
		default : "pending"
	}
});


// [ SECTION ] Models
// Models use Schemas and they act as the middleman from the server to our database Server > Schema (blueprint) > Database > Collection
// "Task" variable can now be used to run commands for interacting with our database; the naming convention for mongoose models follow the MVC format
// the first parameter of the model() method indicates the collection to store the data that will be created
// the second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);


// [ SECTION ] Creation of todo list routes

// Allows our app to read json data
app.use( express.json() );
// Allows our app to read data from forms
app.use( express.urlencoded({ extended:true }));

// Create a new task

/*
	BUSINESS LOGIC:

	1. Check if the ask is already existing in the collection
		- if it exists, return an error/notice. 
		- if it's not, we add it in the database.
	2. The task date will be coming from the request body
	3. Create a new Task object with a "name" field/property.
	4. The "status" property does not need to be provided because our schema defaults it to "pending".
*/
app.post("/tasks", (req, res) => {
	// checking for the duplicate tasks
	Task.findOne({ name : req.body.name }).then((result, err) => {
		// if it exists, return an error/notice.
		if (result != null && result.name === req.body.name){
			return res.send("Duplicate task found");
		// if no document was found
		} else{
			// create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})
			// save the object in the collection
			newTask.save().then((savedTask, error) => {
				// try catch finally can also be used for error handling
				if (error){
					return console.error(error)
				}else{
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})

// Getting all tasks
/*
	BUSINESS LOGIC:

	1. Retrieve all the documents in the collection using find() method. 
	2. If an error is encountered, print the error. 
	3. If no errors are found, send a success (200) status back to the client/postman and return the array of document/result
*/

app.get("/allTasks", (req, res) => {
	Task.find({}).then((tasks, err) => {
		if (err) {
			return res.status(200).send(err);
		}
		if (tasks.length === 0) {
			return res.send("No tasks found in the collection");
		}
		res.send(tasks);
	})
})

/* 

ACTIVITY:

1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a git repository named S35.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.

*/

// Create a User schema.
const userSchema = new mongoose.Schema({
	username : String,
	password: String,

});

// Create a User model.
const user = mongoose.model("User", userSchema);

// Create a POST route that will access the "/signup" route that will create a user.
app.post('/signup', (req, res) => {

    user.findOne({ username : req.body.username }).then((result, err)=>{

        if (result != null && result.username === req.body.username){
            return res.send("Existing user found");
        }
        else if(!req.body.username) 
            return res.send("Input a valid username");
        else if(!req.body.password) 
            return res.send("Input a valid password");
        else{
                    
            let newUser = new user({
                username : req.body.username,
                password : req.body.password
            })

            
            newUser.save().then((savedTask, error) => {
                if (error){
                    return console.error(error)
                }else{
                    return res.status(201).send("New User added")
                }
            })
        }
    })
})



app.listen(port, () => console.log(`Server is running at ${port}.`));























