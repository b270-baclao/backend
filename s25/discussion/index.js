// console.log("Hi, JSON!");

// [SECTION] JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages
	- JSON is used for serializing different data types into bytes
	- Serialization is the process of covering data into a series of bytes for easier transmission/transfer of information
	- Syntax: 
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/

// JSON Objects
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// JSON Arrays

// "cities": [
// 	{
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{
// 		"city": "Manila City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{
// 		"city": "Makati City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	}
// ]

// JASON Methods
// The JSON object contains methods for parsing an coverting data into stringified JSON

// [SECTION] Converting Data Into Stringified JSON
console.warn("Converting Data Into Stringified JSON");
let batchesArr = [
	{
		batchName: "Batch 270"
	},
	{
		batchName: "Batch 271"
	}
]

console.log(batchesArr);
console.log(`Result of stringify method: `);
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});
console.log(data)

// User Details
console.warn("Collecting data via prompt");

// let firstName = prompt("What is your first name?");
// let lastName = prompt("What is your last name?");
// let age = prompt("How old are you?");

// let address = {
// 	city: prompt("Which city do you live in?"),
// 	country: prompt("Which country does your city belong to?")
// };

// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	adress: address
// });
// console.log(otherData);

// [SECTION] Converting Stringified JSON Into JS Objects
console.warn("Converting Stringified JSON Into JS Objects");

let batchesJSON = `[
	{
		"batchName": "Batch 270"
	},
	{
		"batchName": "Batch 271"
	}
]`;
console.log("Result from parse method: ");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "John",
	"age": 31,
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`
console.log(JSON.parse(stringifiedObject));



























































