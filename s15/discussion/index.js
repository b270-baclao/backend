// alert("Hello, Batch 270!");

// This is a statement.

console.log("Hello again!");
console.log("Hi, Batch 270!");

/* 
	- There are two types of comments:
		1. Single-line denoted by two slashes
		2. Multi-line denoted by a slash and asterisk
*/

// [SECTION] Variables
	/*
		- It is used to contain data. 
	*/

// Declaring Variables.
// Syntax: let/const variableName;
/*
	- Trying to print out a value of a variable that has not been declared will return an error of "undefined"
	- Variables must be declared first with value before they can be used.
*/
	let myVariable;
	console.log(myVariable);

	let greeting = "Hello";
	console.log(greeting);
	
// Declaring and Initializing Variables
// Initializing Variables - the instance when a variable is given its initial value
// Syntax: let/const variableName = value;
// let keyword is used if we want to reassign values to our variable
let productName = 'desktop computer'
productName = "Laptop";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

let friend ="Kate";
console.log(friend);

// We cannot re-declare variables within its scope
let friend1 = "Jane";
console.log(friend1);

// let firstName = "Micah";
// console.log (firstName)


// [SECTION] Data Types

// String - series of characters that create a word, a phrase, a sentence or anything related to creating text.
// Strings can be written using either a single or double quote. 
let country = "Philippines"
let province = "Batangas"
console.log(country);
console.log(province);

// Concatenating string
// Multiple string values can be combined to create a single string using "+" plus symbol
let fullAddress = province + ", " + country
console.log(fullAddress);

console.log("I live in the " + country)

// "\n" refers to creating a new line in between texts
let mailAddress = "Metro Manila\nPhilippines"
console.log(mailAddress);

let message = 'John\'s employees went home early';
console.log(message);

// Numbers
	// Integers/Whole Numbers
	let headCount = 26;
	console.log(headCount);

	// Decimal Numbers
	let grade = 98.7;
	console.log(grade);

	// Exponental Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

// Boolean - value is either true or false

let isMarried = false
let inGoodConduct = true
console.log("isMarried: " + isMarried);
console.log("in inGoodConduct: " + inGoodConduct);

// Arrays - are special kind of data type that's used to store multiple data types
// Arrays can store different data types but is normally used to store similar data types
// Syntax: let/const arrayName = [elementa, elementB, ...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
/*
	Syntax:
	let/const ObjectName = {
		propertyA: valueA
		propertyB: valueB
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912345678", "0987654321"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
};
console.log(person)

let myGrades = {
	English: "98.70",
	Math: "92.1",
	Science: "90.2",
	Filipino: "94.6"
}
console.log(grades)

// Null

let spouse = null;
console.log(spouse);





















