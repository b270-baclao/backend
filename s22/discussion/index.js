// console.log("Happy Wednesday!");

// Array Methods
// JavaScript has built-in functions and methods for arrays. this allows us to manipulate and access array items

// [SECITON] Mutator Methods
/*
	- These are methods that "mutate" or change an array after they are created. 
	- These methods manipulate the original array performing various tasks such as adding and removing elements. 
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
/*
	- Adds an element at the end of an array AND returns the new arrays length.
	- Syntax:
		arrayName.push(elementToBeAdded);
*/

console.log("Current fruits array");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength); // 5 - array's length
console.log("Mutated array from push method");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method");
console.log(fruits);

// pop()
/*
	- Removes the last element in an array AND returns the removed element
	- Syntax:
		arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit); //Guava

console.log("Mutated array from pop method: ");
console.log(fruits);

// unshift()
/*
	- Adds one or more elements at the beginning of an array AND returns the new array
	- Syntax:
		arrayName.unshift(elementA, elementB);
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method: ");
console.log(fruits);


// shift()
/*
	- Removeds an element at the beginning of an array AND returns the removed element.
	- Syntax:
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit); //Lime
console.log("Mutated array from shift method: ");
console.log(fruits);

// splice()
/*
	- Simultaneously removes elements from a specified index number and adds elements.
	- Returns the removed element/s.
	- Syntax:
		arrayName.splice(startting index, deleteCount, elementsToBeAdded);
*/

let fruitsSplice = fruits.splice(1, 2, "Lime", "Cherry");
console.log(fruitsSplice); //Apple and Orange - removed elements
console.log("Mutated array from splice method: ");
console.log(fruits);

// sort()
/*
	- Rearranges the array elements in alphanumeric order
	- Syntax:
		arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);

// reverse()
/*
	- Reverses the order of the array elements
	- Syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log("Mutated array from reverse method: ");
console.log(fruits);


// Non-mutator Methods
/*
	- Non-mutator methods are methods that do not modify or change an array after they're created
	- These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing outputs

*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

// index.of()
/*
	- Returns the index number of the first matching element found in an array
	- If no match was found, it will return -1
	- The search process will start from the first element proceeding to the last element
	- Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);

*/

let firstIndex = countries.indexOf("PH", 2);
console.log("Result of indexOf method: " + firstIndex); //5

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry); //-1

// lastIndexOf()
/*
	- Returns the index number of the last matching element found in the array
	- The search process wiil be done from the last element proceeding to the first element
	- Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);

*/
let lastIndex = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndex); //1


// slice()
/*
	- Portions/sices elements from an array and returns the new array
	- Syntax:
		arrayName.slice(startingIndex);
*/

let slicedArrayA = countries.slice(2);
console.log(slicedArrayA); // ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

// Slicing of elements starting a specified index to another index
let slicedArrayB = countries.slice(2, 5); //index 2, 3, 4 //from index 2 to 4 not including the end point (index 5)
console.log(slicedArrayB); //

// Slicing of elements starting from the last element of an array
let slicedArrayC = countries.slice(-3); 
console.log("Result from slice method: ");
console.log(slicedArrayC); //['PH', 'FR', 'DE'] last 3 elements


// toString()
/*
	- Returns an array as a sting separated by commas

*/

let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray); // US,PH,CAN,SG,TH,PH,FR,DE


// concat()
/*
	- Combines two arrays and returns the combined result/array
	- Syntax:
		arrayName.concat(arrayB);
*/

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe bootstrap"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method:"); 
console.log(tasks); // ['drink html', 'eat javascript', 'inhale css', 'breathe bootstrap']



// Combining multiple arrays

console.log("Result from concat method:");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks); // ['drink html', 'eat javascript', 'inhale css', 'breathe bootstrap', 'get git', 'be node']

// Combining arrays with elements

let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log(combinedTasks); // ['drink html', 'eat javascript', 'smell express', 'throw react']

//join()
/*
	- Returns an array as a string separated by a specified separator
	- Syntax:
		arrayName.join("separator");
*/

let users = ["John", "Jane", "Joe", "Joshua"];

console.log(users.join());
console.log(users.join(" "));
console.log(users.join(" - "));


// [SECTION] Iteration Methods

// forEach()
/*
	- Similar to a for loop that iterates on each array element
	- For each item in the array, the anonymous function passed in the forEach() wil be run.
	- The anonymous function is able to receive the current item being iterated or looped over by assigning a parameter. 
	- Does not return anything
	- Syntax:
		arrayName.forEach(function(indivElement) {
			statement
		})
*/

let filteredTasks = [];

allTasks.forEach(function(task) {

	if(task.length > 10) {

		filteredTasks.push(task);
	}
})

console.log("Result of forEach tasks: ");
console.log(filteredTasks); // ['eat javascript', 'breathe bootstrap']


// map()
/*
	- Iterates on each element and returns a new array.
	- Syntax:
		arrayName.map(function(indivElement) {
			statement
		})
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
	return number * number;
})
console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method: ");
console.log(numberMap);

// map() vs forEach ()

let numbersForEach = numbers.forEach(function(number) {
	return number * number;
})
console.log(numbersForEach); //undefined - because forEach() does not return anything


// every()
/*
	- Checks if all elements in an array meet the given condition
	- Returns a true value if all elements meet the condition and false if otherwise.
*/

let allValid = numbers.every(function(number) {
	return (number < 3);
})
console.log("Result of every method:");
console.log(allValid); //false

//some()
/*
	- Checks if at least 1 element in the array meets the given condition
	- Returns a true value if some elements meet the condition and false if otherwise.
*/

let someValid = numbers.some(function(number) {
	return (number < 10);
})
console.log("Result of some method:");
console.log(someValid);


// filter()
/*
	- Returns a new array that contains the elements which meet the given condition
	- Returns an empty array if no elements were found
*/

let filterValid = numbers.filter(function(number) {
	return (number < 3); // [1, 2]
})
console.log(filterValid);


// includes()
/*
	- Checks if the argument passed can be found in the array
	- It returns a boolean value
*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1); //true

let productFound2 = products.includes("Headset");
console.log(productFound2); //false


// reduce()
/*
	- Evalues elements from left to right and returns/reduces the array into a single value
*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {

	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("currentValue: " + y);

	return x + y;
})
console.log("Result of reduced method: " + reducedArray);
 

let list = ["Hello", "Again", "World"]

// 1. x = Hello, y = Again => Hello Again

// 2. x = Hello Again = y = World => Hello Again World - reduced value


let reducedString = list.reduce(function(x, y) {

	return x + " " + y;
})
console.log("Result of reduce method: " + reducedString); // Hello Again World








































