/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	// first function here:

	function printUserDetails(){
		let fullName = prompt("Enter your full name.");
		let age = prompt("Enter your age.");
		let location = prompt("Enter your location.");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}
	printUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function topFiveMusicalArtist(){
		let topOneBand = "1. The Beatles";
		let topTwoBand = "2. Metallica";
		let topThreeBand = "3. The Eagles";
		let topFourBand = "4. L'arc~en~Ciel";
		let topFiveBand = "5. Eraserheads";

		console.log(topOneBand);
		console.log(topTwoBand);
		console.log(topThreeBand);
		console.log(topFourBand);
		console.log(topFiveBand);
	}
	topFiveMusicalArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function topFiveMoviesOfAllTime(){
		let topOneMovie = "1. The Godfather";
		let topTwoMovie = "2. The Godfather, Part II";
		let topThreeMovie = "3. Shawshank Redemption";
		let topFourMovie = "4. To Kill A Mockingbird";
		let topFiveMovie = "5. Psycho";

		console.log(topOneMovie);
		console.log("Rotten Tomatoes Rating: 97%")
		console.log(topTwoMovie);
		console.log("Rotten Tomatoes Rating: 96%")
		console.log(topThreeMovie);
		console.log("Rotten Tomatoes Rating: 91%")
		console.log(topFourMovie);
		console.log("Rotten Tomatoes Rating: 93%")
		console.log(topFiveMovie);
		console.log("Rotten Tomatoes Rating: 96%")
	}
	topFiveMoviesOfAllTime();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// Function Expression
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();






