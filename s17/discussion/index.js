// console.log("Hello World")

// Functions
	// Functions in JS are lines/blocks of codes that can tell our device/application to perform a certain task when called or invoked. 

// Function Declarations
	// function statement - defines a function wtih a specified parameter. 

	/* Syntax:
		function functionName() {
			code black (statement)
		}

		- function keyword - used to define a JS function
		- functionName - is the function's name. Functions are named to be able to be used or called later in our code.
		- function block ({}) - the statement which comprise the body of the function. This is where the code to be executed is written.
	*/

	function printName() {
		console.log("Hi! My name is Micah.")
	};

	// Function Invocation

	printName();

	// Semicolons are used to separate executable JS statements or codes.

// Function Declaration VS Function Expressions

	// 1. Function Declaration
	declaredFunction();

	function declaredFunction() {
		console.log("Hello World from declaredFunction()")
	}

	declaredFunction();

	// 2.) Functon Expression

		// A function can also be stored in a variable.
		// A function expression is anonymous functon assigned to the variableFunction


		/*
			let variableName = function () {
				code block (statement);
			}
		*/

		// let n = 30; //this is how we initialize a variable

		// variableFunction();

		let variableFunction = function() {
			console.log("Hello again, Batch 270!")
		};

		variableFunction();

		// We invoke the function expression using its variable name, not its function name.
		let funcExpression = function funcName() {
			console.log("Hello from the other side.")
		};
		funcExpression();

		// You can reassign declared functions and function expressions to a new anonymous functions.

		declaredFunction = function() {
			console.log("Updated declaredFunction")
		};

		funcExpression = function() {
			console.log("Updated funcExpression")
		};

		declaredFunction();
		funcExpression();

		const constantFunc = function() {
			console.log("Initialized with const");
		}

		constantFunc();
		declaredFunction();

/*		constantFunc = function() {
			console.log("Cannot be re-asigned");
		}

		constantFunc();

*/

// Function Scoping

/* 
	Scope is the accessibility (visivility) of variables within our program.

		JavaScript Variables has 3 types of scope;
			1. local/block scope
			2. global scope
			3. function scope

*/
		// 1. Local Scope
	 	{
	 		let localVar = "Armando Perez";
	 	}

	 	// 2. Glocal Scrope
	 	let globalVar = "Mr. World";

	 	console.log(globalVar);
	 	// console.log(localVar);

	 	// 3. Function Scope

	 		function showNames() {
	 			// function scope variables
	 			var functionVar = "Joe";
	 			const functionConst = "John";
	 			let functionLet = "Jane";

	 			console.log(functionVar);
	 			console.log(functionConst);
	 			console.log(functionLet);
	 		}

	 		showNames();

	 		// console.log(functionVar); - this will result to an error.
	 		// console.log(functionConst); - this will result to an error.
	 		// console.log(functionLet); - this will result to an error.


	// Nested Functions
	// We can create another function inside a function.This is called a nested function. This neted function, being inside the myNewFunction will have access to the variable name, as we are within the same scope/code block. 

	 function myNewFunction(){
	 	let name = "Jane";

	 	function nestedFunction(){
	 		let nestedName = "John";
	 		console.log(name);
	 		console.log(nestedName);
	 	}
	 
	 	nestedFunction();
	 }

	// nestedFunction(); results to an error
	myNewFunction();

	// Function and Global Scope Variables

	let globalName = "Alexandro";

	function myNewFunction2(){
		let nameInside = "Renz";
		console.log(globalName);
	}

	myNewFunction2();
	// console.log(nameInside); results to an error


// Using alert()

	// Syntax: alert ("<message in string>")
	// alert("Hello World!"); // This will run immediately when the page loads

	function showSampleAlert(){
		alert("Hello, User!");
	}

	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed.")

// Using prompt()

	// Syntax: prompt("<dialog in string>")

	// let samplePrompt = prompt("Enter your name.");
	// console.log("Hello, " + samplePrompt);

	// console.log(typeof samplePrompt);

	// let sampleNullPrompt = prompt("Don't enter anything.");
	// console.log(sampleNullPrompt); //returns an empty string when there is no input or null if the user cancels the prompt. 

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name.");
		let lastName = prompt("Enter your last name.");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
	}
	printWelcomeMessage();

// Function Naming Conventions
	// 1. Function names should be descriptive of the task that it will perform. It usually contains a verb

	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}
	getCourses();

	// 2. Avoid generic names to avoid confusion within your code.

	function get(){
		let name = "Jamie";
		console.log(name);
	}
	get();

	// 3. Avoid pointless and inappropriate function names.

	function foo(){
		console.log(25%5);
	}
	foo();

	// 4. Name your functions using camelCasing

	function displayCarInfo (){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");

	}
	displayCarInfo();











