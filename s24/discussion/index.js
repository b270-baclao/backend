// console.log("Hello, Batch 270!")

// [SECTION] ES6 Updates
// ES6 is also knows as ECMAScript 2015 is an  update to the previous versions of ECMAScript
// ECMA - European Computer Manufacturers Assiciation
// ECMAScript is the standard that is used to create implementation of the language, one which is JavaScript

// New Features of JavaScript

// [SECTION] Exponent Operator
// Using the exponent operator
const anotherNum = 8 ** 2;
console.log(anotherNum);

// Using Math object methods
const num = Math.pow(8, 2);
console.log(num); //64


// [SECTION] Template Literals

// Pre-Template Literals
// Uses single/double quotes (""/'')
let name = "Micah";

let message = "Hello, " + name + "!\nWelcome to programming!";
console.log(message);

// String Using Template Literals
// Uses backticks(``) instead of('') or ("")

message = `Hello, ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// Multi-line Using Template Literals
const anotherMessage = `${name} attended a math competition. 

He won it by solving the problem 8 ** 2 with the solution of ${anotherNum}.`
console.log(anotherMessage);

/*
	- Template literals allows us to write strings with emebeded JavaScript expressions
	- "${}" are used to include the JS expressions
*/

const interestRate = .5;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);


// [SECTION] Array Destructuring
/*
	- Allows us to unpack elements in arrays into distunct variables
	- Allows us to name arrat elements with variables instead of using indes numbers
	- Syntax:
		let/const [variableNameA, variableNameB, variableNameC] = array;
*/

const fullName = ["Micah", "Angelica", "Baclao"];

console.warn("Pre-Array Destructuring:");
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Expected Output: Hello, Micah Angelica Baclao! It's nice to meet you!
console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);


console.warn("Array Destructuring:");
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(fullName);

console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);


// [SECTION] Object Destructuring
console.warn("Object Destructuring");
let person = {
	givenName: "Jane",
	midName: "Dela",
	familyName: "Cruz"
}

console.log(person.givenName);
console.log(person.midName);
console.log(person.familyName);

console.log(`Hello, ${person.givenName} ${person.midName} ${person.familyName}! It's good to see you!`);

// Object Destructuring Mini Activity
let {givenName, midName, familyName} = person;

console.log(givenName);
console.log(midName);
console.log(familyName);

console.log(`Hello, ${givenName} ${midName} ${familyName}! It's good to see you!`);


// [SECTION] Arrow Functions
/*
	- Compact alternative to traditional functions
	- This will only work with "function expressions"
	- Syntax:
		let/const variableName = () => {
			//code block/statement
		}

		let/const varibleName = (parameter) => {
			//code block/statement
		}

*/

// function declaration
function greeting() {
	console.log("Hello World!");
}
greeting();

// function expression

const greet = function() {
	console.log("Hi, World!")
}
greet();

// Arrow function without Parameter

const greet1 = () => {
	console.log("Hello Again!");
}
greet1();

// Arrow Function with Multiple Parameters

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("John", "D", "Smith");

// Arrow Functions with Loops
const students = ["John", "Jane", "Judy"];

// Pre-Arrow Function
students.forEach(function(student) {
	console.log(`${student} is a student.`);
})

// Arrow Function

students.forEach((student) => {
	console.log(`${student} is a student.`);
})

// Another example
// An arrow function has an implicit return value when the curly braces are omitted.
const add = (x, y) => { return x + y};


let total = add(1, 2);
console.log(total);

// [SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when hte function is invoked.
console.warn("Default Function Argument Value");

const greet2 = (name = "User") => {
	return `Good morning, ${name}!`;
}
console.log(greet2());
console.log(greet2("Callie"));


// [SECTION] Class-Based Object Blueprints
console.warn("Class-Based Object Blueprints");
/*
	- The "constructor" is a special method of a class for creating/initializing an object for that class
	- Syntax: 
		class className {
			contructor(objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Mustang";
myCar.year = "2021"
console.log(myCar);

// Instantiating a new object with initialized values
const myNewCar = new Car("Toyota", "Fortuner", 2020);
console.log(myNewCar);


































