// console.log("Hello World!");

// Arithmetic Operators

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = y / x;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// Assignment Operators

	// Nasic Assignment Operator(=)
	let assignmentNumber = 8;

	// Addition Assignment Operatiom(+=)
	// assignmentNumber = assignmentNumber + 2;
	// console.log("Result of assignment operator: " + assignmentNumber);

	// Shorthand for assignmentNumber = assignmentNumber + 2;
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses

	// The operations were done in the following order:

	let number = 1 + 2 - 3 * 4 /5;
	console.log("Result of operator: " + number);

	// The operations were done in the following order:
	// 1. 4 / 5 = 0.8
	// 2. 2 - 3 = -1
	// 3. -1 * 0.8 = -0.8
	// 4. 1 + -0.8 = 0.2
	let pemdas = 1 + ( 2 - 3 ) * ( 4 / 5);
	console.log("Result of operator: " + pemdas);


// Type Coercion - different date types

	let numA = "10";
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	let noCoercion = numC + numD;
	console.log(noCoercion);
	console.log(typeof noCoercion);

	// The result is a number
	// The "true" is also associated with the value of 1
	let numE = true + 1;
	console.log(numE);

	// The "false" is also associated with the value of 0	
	let numF = false + 1;
	console.log(numF);

	// Comparison Operators
	let juan = "juan";

	// Equality Operator (==)
	/*
		- Checks whether the operands are equal/have the same content
		- Returns a boolean value
	*/
	console.log("Equality Operator");
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == "1"); //true
	console.log(0 == false); //true
	console.log("juan" == "juan") //true
	console.log("juan" == juan); //true


	// Inequality Operator
	console.log("Inequality Operator");
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != "1"); //false
	console.log(0 != false); //false
	console.log("juan" !="juan") //false
	console.log("juan" != juan); //false

	// Strict Equality Operator
	/*
		- Checks whether the operands are equal/have the same content
		- Also compares the data types of teh 2 values
		- Returns a boolean value
	*/
	console.log("Strict Equality Operator");
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === "1"); //false
	console.log(0 === false); //false
	console.log("juan" === "juan") //true
	console.log("juan" === juan); //true

	console.log("Strict Inequality Operator");
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== "1"); //true
	console.log(0 !== false); //true
	console.log("juan" !== "juan") //false
	console.log("juan" !== juan); //false

// Relational Operators
console.log("Relational Operators");
	
	let a = 50;
	let b = 65;

	// Greater than operator ( > )
	let isGreaterThan = a > b;
	console.log(isGreaterThan); //false

	// Less than operator ( < )
	let isLessThan = a < b;
	console.log(isLessThan); //true

	let isGtOrEqual = a >= b;
	console.log(isGtOrEqual); //false

	let isLOrEqual = a <= b;
	console.log(isLOrEqual); //true

// Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&&)
	// Returns true if al operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " + allRequirementsMet);


	// Logical OR Operator (||)
	// Returns true if one of the operands is true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);

	// Logical NOT Operator (!)
	// Returns the opposite value
	let someRequirementsNotMet = !isLegalAge
	console.log("Result of logical NOT operator: " + someRequirementsNotMet);


// Increment (++) and Decrement (--)

let z = 1;

// Pre-increment

// The value of "z" is added by values of 1 before returning the value and storing it in the variable
// Increment first before returning the valuw
let increment = ++z;
console.log("Result of pre-increment: " + increment); //2
console.log("Result of pre-increment: " + z); //2

// Post-increment
// The value of "z" is returned first and stored in the variable "increment" then the value of "z" is increased by 1
// Returns the value first before incrementation
increment = z++;

// The value of "z" is at 2 before it was incremented
console.log("Result of post-increment: " + increment); //2
console.log("Result of post-increment: " + z); //3

// Pre-Decrement
let decrement = --z;
console.log("Result of pre-decrement" + decrement);
console.log("Result of pre-decrement" + z);

// Post-Decrement
decrement = z--;
console.log("Result of post-decrement" + decrement);
console.log("Result of post-decrement" + z);





































