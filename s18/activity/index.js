

// 1.  Create a function which will be able to add two numbers.
// 	-Numbers must be provided as arguments.
// 	-Display the result of the addition in our console.
// 	-function should only display result. It should not return anything.


// 	Create a function which will be able to subtract two numbers.
// 	-Numbers must be provided as arguments.
// 	-Display the result of subtraction in our console.
// 	-function should only display result. It should not return anything.

// 	-invoke and pass 2 arguments to the addition function
// 	-invoke and pass 2 arguments to the subtraction function


	function sum(a, b) {
		let sumNum = (a + b);
		console.log("Displayed sum of 5 and 15");
		console.log(sumNum);
	} 
	sum(5, 15);
		

	function difference(x, y) {
		let differenceNum = (x - y);
		console.log("Displayed difference of 20 and 5");
		console.log(differenceNum);
	} 
	difference(20, 5);



// 2.  Create a function which will be able to multiply two numbers.
// 		-Numbers must be provided as arguments.
// 		-Return the result of the multiplication.

// 		Create a global variable called outside of the function called product.
// 		-This product variable should be able to receive and store the result of multiplication function.

// 		Log the value of product variable in the console.


	function displayedProduct(numX, numY) {
		let product = numX * numY;
		console.log("The product of " + numX + " and " + numY + ":");
		return product;
	} 
		let product = displayedProduct(50, 10) ;
		console.log(product);


// 	Create a function which will be able to divide two numbers.
// 		-Numbers must be provided as arguments.å
// 		-Return the result of the division.

//		Create a global variable called outside of the function called quotient.
// 		-This quotient variable should be able to receive and store the result of division function.

// 		Log the value of quotient variable in the console.

	function displayedQuotient(numX, numY) {
		let quotient = numX / numY;
		console.log("The quotient of " + numX + " and " + numY + ":");
		return quotient;
	} 
		let quotient = displayedQuotient(50, 10) ;
		console.log(quotient);


// 3. 	Create a function which will be able to get total area of a circle from a provided radius.
// 		-a number should be provided as an argument.
// 		-look up the formula for calculating the area of a circle with a provided/given radius.
// 		-look up the use of the exponent operator.
// 		-you can save the value of the calculation in a variable.
// 		-return the result of the area calculation.

// 	Create a global variable called outside of the function called circleArea.
// 		-This variable should be able to receive and store the result of the circle area calculation.

// 	Log the value of the circleArea variable in the console.

		function areaCalculation(radius){
            console.log("The result of getting the area of a circle with " +radius + " radius:")
            return 3.1416 * Math.pow(radius, 2);
        }
        let circleArea = areaCalculation(15);
        console.log(circleArea)


// 4. 	Create a function which will be able to get total average of four numbers.
// 		-4 numbers should be provided as an argument.
// 		-look up the formula for calculating the average of numbers.
// 		-you can save the value of the calculation in a variable.
// 		-return the result of the average calculation.

//     Create a global variable called outside of the function called averageVar.
// 		-This variable should be able to receive and store the result of the average calculation
// 		-Log the value of the averageVar variable in the console.


    function getAaverage(numA, numB, numC, numD) {
    	return (numA + numB + numC + numD) / 4  
    }
      let averageVar = getAaverage(20, 40, 60, 80);
      console.log("The average of 20, 40, 60, 80:")
      console.log(averageVar);


// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
// 		-this function should take 2 numbers as an argument, your score and the total score.
// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
// 		-return the value of the variable isPassed.
// 		-This function should return a boolean.

// 	Create a global variable called outside of the function called isPassingScore.
// 		-This variable should be able to receive and store the boolean result of the checker function.
// 		-Log the value of the isPassingScore variable in the console.

      function checkIfPassed(score, total) {
      	return (score / total) * 100 > 75;
      }
      let passingScore = checkIfPassed(38, 50);
      console.log("Is 38/50 a passing score?");
      console.log(passingScore);





