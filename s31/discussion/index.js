// Use the "require" directive to load Node.js modules
// A module is a software component or part of a program that contains one or more routines/functions.
// "http" is a NODE JS module that lets the NODE JS transfer data using Hyper Text Transfer Protocol
// Provides a functionality for creating HTTP servers and clients, allowig applications to spend and receive HTTP requests.

// Check https://www.techopedia.com/definition/3843/module

let http = require("http");

// Using this module's createServer() method, we can create an HTTP service that listens to requests on a specified port and gives responses back to the client

// The messages sent by the client, usually a web browser, are called "requests".
// The messages sent by the server as an answers are called responses. 

// The server will be assigned to port 4000 via "listen(4000)" method where the server will listedn to any requests that are sent to it.
// A port is a virtual point where network connections start and end.
http.createServer(function (request, response) {

	// Use the writeHead() method to:
		// Set a status code for the response - 200 means OK
		// Set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// Send the response with the text content "Hello World!"
	response.end("Hello World!")

}).listen(4000);

// When server is running, console will print the message:
console.log("Server is running at localhost:4000");