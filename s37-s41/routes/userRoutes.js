const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth")

// Route for checkinng if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller to communicate with our database
router.post("/checkEmail", userController.checkEmailExists);

// Route for user registration
router.post("/register", userController.registerUser);

// Route for user authentication
router.post("/login", userController.loginUser);

// Route that will accept the user’s Id to retrieve the details of a user.
router.get("/details", auth.verify, userController.getProfile);

// Route for enrolling a user to a course
router.post("/enroll", auth.verify, userController.enroll); 

module.exports = router;